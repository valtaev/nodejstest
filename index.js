var Twit = require('twit');
var ent = require('ent');
var express = require('express');
var app = express();

app.set('port', (process.env.PORT || 5000));

app.use(express.static(__dirname + '/public'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get('/', function(request, response) {
  response.render('pages/index');
});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});


//your twitter account @name
var your_twitter_name = "na_xui";

// set up our two data sources (you can paste these in your browser to see the results, in JSON format)

// URI for all recent #search_query tweets
var search_query = {q: "фотоконкурс", count: 100, result_type: "recent"}; 

// URI for all recent @na_xui  tweets
//NOTE: BETTER WAY NOT USE SEARCH TOOL BACAUSE NOT ALL TWEETS IS AVAILABLE FOR SEARCH

// !!! Don't forget to change screen name to yours ;)
var tweets_query = {screen_name: your_twitter_name, count: 100};

//black list for tweet search result
  	
var ignoreList = new Array('@', 'RT', 'продолжается', 'прошли отборочный', 'состоялся', 'завершился', 'к завершению', 'подведение итогов', 'участник', 'для конкурса', '&amp;gt;', '&gt;', '&amp;', 'quot;', 'айкните', 'проголосуйте', 'хочу поучаствовать', '2013');


// -------------------------------------------------
//
//     FUNCTIONS
//
// -------------------------------------------------

		//	compare by pattern "[word фотоконкурс] and the next word"
		//	@ str - string
		//	@ return array or null
		function patternCheck(str){
		
			//	LEGEND: (first letter)(rest_of_word_lowercase OR Uppercase)(comma or : or . or ... MAYBE) SPACE (second word with special chars "«#)
			var return_str = str.match(/(Ф|ф|#ф)(отоконкурс|ОТОКОНКУРС)((,|:|.|...)?) ([\w.:,А-Яа-я"“«#]+)/g); 
			return return_str;
		
		}
		
		//	compare string with array elements and find same strings
		// @str_val - string
		// @arr - array of twitted elements
		// @return boolean
		function is_str_exist(str_val, arr){

		var is_exist = false;

			//	if we have an array
			if(arr.length){
				for (var i = 0; i < arr.length; i++) {
					//	get text part
					var arr_el = arr[i].text;
					//	strip urls, because is possibel to find same tweets with different urls
					arr_el = arr_el.replace(/(https?:\/\/[^\s]+)/g, '');					
					str_val = str_val.replace(/(https?:\/\/[^\s]+)/g, '');
					
					//	check if text match
					var arr_valid = patternCheck(arr_el); 
					
					if(arr_valid){
						//	check if text match
						var str_valid = patternCheck(str_val); 
						
						if(str_valid){
							//	lowercase both strings to best match
							var arr_valid_lower = arr_valid[0].toLowerCase();
							var str_valid_lower = str_valid[0].toLowerCase();
							//	compare
							if(arr_valid_lower.localeCompare(str_valid_lower) == 0){
								//	gotcha
								is_exist = true;
								break;
							}
						}else{
							//LOGGING DISABLED BECAUSE FILTER IS APPLY ON NEW SEARCH RESULT FILTERING
							//console.log("??? pattern not match with: "+str_val);
						}
					}else{
					//LOGGING DISABLED BECAUSE FILTER IS APPLY ON NEW SEARCH RESULT FILTERING
					//console.log("!!! pattern not match with: "+arr_el);
					}
				}
			}
			return is_exist;
		}
		
		//	ignore list for twitter search results
		//	@searchTerm text
		//	@return number if match or -1
		function ignoreListIndexOf(searchTerm) {
			
			for(var i = 0, len = ignoreList.length; i < len; i++) {
				
				var srch = searchTerm.indexOf(ignoreList[i]);
				
				if(srch != -1){
					return srch;
					break;
				}				
			}
			
			return -1;
			
		}

// -------------------------------------------------
//
//     ENGINE
//
// -------------------------------------------------

function searchAndTweet() {
	
	//	timestamp
	var d = new Date();
	var timestamp = d.toUTCString();
	// filtered search results array
	var konkurses = [];	
	//	tweet string empty by default
	var tweet = "";
	// count of success items after ignoreList filter
	var success_cnt = 1;
	
	//	twitter search
	// get the search_query tweets 
  T.get('search/tweets', search_query, function(err, data) {
    // Stop if we receive an error instead of search results
    if (err) throw err    
	
	// jettison the metadata, we just care about the results of the search
    var results = data.statuses;
	
	console.log("========== NEW=========="); 
	
	//	log to file
	var fs = require('fs');
	var stream = fs.createWriteStream("log.txt");
	stream.once('open', function(fd) {
		
		
		// look at each result and push it to an array called 'konkurses' if it is not an RT and ' and ' appears
		
		for (var i = 0; i < results.length; i++) {
		  var text = results[i].text;
		  
		  var nu_url_text = text.replace(/(https?:\/\/[^\s]+)/g, '');	
			
			// filter results with ignoreList array
			//	ignore tweets with words in list
			// ignore non-url tweets
			// ignore tweets that not match to pattern(checking without url)
			if (ignoreListIndexOf(text) == -1 && text.indexOf('http://') != -1 && patternCheck(nu_url_text)) {  
				   
				var str = (success_cnt)+". "+text +"\n";
				
				stream.write(str);
				konkurses.push(text);
				
				console.log(str); //+" : "+konkurses.length
				success_cnt++;
			}
			/*else{
			console.log("t="+text+" r="+ignoreListIndexOf(text)+" h="+text.indexOf('http://'));
			}*/		 
		}		
		 stream.end();		 	 
	});
	

    // get the tweets_query tweets
	
	   // T.get('search/tweets', tweets_query, function(err, data) {
	   T.get('statuses/user_timeline', tweets_query, function(err, data) {
			  if(err) throw err;

				//console.log("Find records:"+konkurses.length);
				var gotTweets = data;
				console.log("Bot made "+gotTweets.length+" tweets");
				
				if(gotTweets.length>=1){		
					
					var k_len = konkurses.length*2;
					//get random tweets and find non-twitted item
					for (var i = 0; i < k_len; i++) {
						var genId = Math.floor(Math.random() * konkurses.length);
						var t = konkurses[genId];	 	
						//console.log("--TRY: "+t+" Left: "+konkurses.length);
						// if selected tweet not used before, take it!
						if(!is_str_exist(t,gotTweets)){
							console.log("-=---=----=-");
							tweet = t;	 
							//remove line breaks
							tweet = tweet.replace(/(\r\n|\n|\r)/gm,"");
							tweet = tweet.substr(0, 140);
							console.log(tweet);
							console.log(tweet.length+" :: "+ timestamp);

							break;
							
						}else{
							// remove used element from array;
							konkurses.splice(genId,1);
							// stop this, nothing new...
							if(konkurses.length == 0) break;
						}
					}
					
				}else{
					// first run - any random tweet
					tweet = konkurses[Math.floor(Math.random() * konkurses.length)];	 
					// remove line breaks
					  tweet = tweet.replace(/(\r\n|\n|\r)/gm,"");
					  tweet = tweet.substr(0, 140);
					  console.log("---------");
					  console.log(tweet);
					  console.log(tweet.length+" :: "+ timestamp);
				}
				/*
				//DEBUG
				//record twieets to file
				var fs = require('fs');
				var stream = fs.createWriteStream("tweets.txt");
				stream.once('open', function(fd) {
					
					
					// look at each result and push it to an array called 'konkurses' if it is not an RT and ' and ' appears
					// more than 20 characters into the tweet
					
					for (var i = 0; i < gotTweets.length; i++) {
							var text = gotTweets[i].text;				   
							var str = text +'","';				
							stream.write(str);	 
					}
					
					 stream.end();
				});
							//end DEBUG
				*/
				if(tweet != ''){
					//tweet if we have something new
					/**/
					T.post('statuses/update', {
						status: tweet
					 }, function (err, reply) {}); 	
					
				
				}else{
					console.log("Oops... Nothing new to post... :( "+" :: "+ timestamp);
				}
			  

		});
		
  });
}

// -------------------------------------------------
//
//    SET UP AND START
//
// -------------------------------------------------



// Our twitter developer API info is in a file called 'config.js'
var T = new Twit(require('./config.js'));


// Tweet once when we start up the bot
searchAndTweet();

// Tweet every 15 minutes
setInterval(function () {
  try {
    searchAndTweet();
  }
  catch (e) {
    console.log(e);
  }
}, 1000 * 60 * 15);
